from PIL import Image
from tqdm import tqdm
import strgen  
import os
import time
import shutil 
    

ran1=[]
ran2=[]

leng=len(os.listdir("."))

for i in range(leng-1):
	ran2.append(strgen.StringGenerator("[\w\d]{4}").render())

#ran2=ran1.copy()
for i in range(70):
	ran1.append(strgen.StringGenerator("[\w\d]{4}").render())

path2="/home/bspwm/pillTest/"

l=[]
fn2=[]
for filename in os.listdir("."):
	
	l.append(filename)


for fname in l:
	if fname != "pill.py":
		newPath2=path2+fname
		fn2.append(newPath2)


print(fn2)

def rezize(im1):

	newsize = (100, 100) 
	im1 = im1.resize(newsize) 
	return im1
	
def saveImg(img1):
	
	st=ran1.pop()
	strg=st+".png"
	img1.save(strg)  

i=1
j=1

globalDirName=[]
for singlePath in tqdm(fn2):
	
	
	
	os.chdir(singlePath)
	mkdirName=ran2.pop()
	globalDirName.append(mkdirName)
	os.mkdir(mkdirName)

	for filename2 in os.listdir("."):
		

		slash="/"+mkdirName

		stn=singlePath+"/"+filename2
		stn1=singlePath+"/"+mkdirName
		if stn!=stn1:
			im = Image.open(str(stn))
			rez1=rezize(im)
			innerPath=singlePath+slash
			os.chdir(innerPath) 
			saveImg(rez1)
			os.chdir(singlePath)
			j=j+1
	i=i+1

print(i,j)
print(globalDirName)

os.chdir(path2)
os.mkdir("dest")


# Source path  
src = '/home/bspwm/pillTest/'
   
# Destination path  
dest = '/home/bspwm/pillTest/dest/'

globalName=[]
for fname2 in fn2:
	os.chdir(fname2)
	lst=os.listdir(".")
	for name in globalDirName:
		for l in lst:
			if name in l:
				globalName.append(name)
				break
		#break
	d=globalName.pop()
	sour=fname2+"/"+d
	shutil.move(sour, dest)
 
